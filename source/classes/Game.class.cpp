/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Game.class.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 11:26:34 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/25 10:01:11 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "nibbler.hpp"
#include "Game.class.hpp"
#include "Player.class.hpp"
#include "Exceptions.hpp"

//start canonical form
Game::Game(void)
{
	this->score = 0;
	return ;
}

Game::Game(std::string size_x, std::string size_y)
{
	this->score = 0;
	this->win_x = stoi(size_x);
	this->win_y = stoi(size_y);
	return ;
}

Game::Game(Game const &src)
{
	*this = src;
	return ;
}

Game::~Game(void)
{
	if (this->lib1_handle)
	{
		dlclose(this->lib1_handle);
	}
	return ;
}

Game	&Game::operator=(Game const &rhs)
{
	if (this != &rhs)
	{
		this->win = rhs.win;
		this->score = rhs.score;
	}
	return (*this);
}
//end canonical form

int		Game::getInit(void) const
{
	if (this->win)
		return (1);
	return (0);
}

void	Game::check_and_load_libraries(void)
{
	std::string dlerror_msg;

	this->lib1_handle = dlopen(library1, RTLD_LAZY);
	if (!this->lib1_handle)
		throw Exceptions::LibraryDoesNotExist("libDynamicLibrary1.so");

	this->print_string1 = (void(*)(const std::string)) dlsym(this->lib1_handle, "SayHelloString");
	if (!this->print_string1)
	{
		dlerror_msg = dlerror();
		if (!this->lib1_handle)
			dlclose(this->lib1_handle);
		throw Exceptions::LibraryFunctionDoesNotExist("SayHelloString", dlerror_msg);
	}

	this->lib2_handle = dlopen(library2, RTLD_LAZY);
	if (!this->lib2_handle)
	{
		throw Exceptions::LibraryDoesNotExist("libDynamicLibrary2.so");
	}
	this->print_string2 = (void(*)(const std::string)) dlsym(this->lib2_handle, "SayHelloString");
	if (!this->print_string2)
	{
		dlerror_msg = dlerror();
		if (!this->lib1_handle)
			dlclose(this->lib1_handle);
		if (!this->lib2_handle)
			dlclose(this->lib2_handle);
		throw Exceptions::LibraryFunctionDoesNotExist("SayHelloString", dlerror_msg);
	}

	this->lib3_handle = dlopen(library3, RTLD_LAZY);
	if (!this->lib3_handle)
	{
		throw Exceptions::LibraryDoesNotExist("libDynamicLibrary3.so");
	}
	this->print_string3 = (void(*)(const std::string)) dlsym(this->lib3_handle, "SayHelloString");
	if (!this->print_string3)
	{
		dlerror_msg = dlerror();
		if (!this->lib1_handle)
			dlclose(this->lib1_handle);
		if (!this->lib2_handle)
			dlclose(this->lib2_handle);
		if (!this->lib3_handle)
			dlclose(this->lib3_handle);
		throw Exceptions::LibraryFunctionDoesNotExist("SayHelloString", dlerror_msg);
	}
}

void	Game::init(void)
{
	check_and_load_libraries();
	this->win = initscr();
	noecho();
	//access keys
	keypad(this->win, TRUE);
	//no buffer on key press
	nodelay(this->win, true);
	//hide the cursor
	curs_set(0);
	if (!has_colors())
	{
		endwin();
		throw Exceptions::TerminalHasNoColour();
	}
	start_color();
	attron(A_BOLD);
	box(this->win, 0, 0);
	attroff(A_BOLD);
}

static int	GameMenu(std::string menuType)
{
	int			read_char = 0;
	int			x, y;
	std::string	newgame = menuType;
	std::string	quitgame = "QUIT";
	int			start_quit = 0;

	init_pair(1, COLOR_BLUE, COLOR_RED);
	while (1)
	{
		wclear(stdscr);
		getmaxyx(stdscr, y, x);
		
		move(y / 2, (x / 2) - (newgame.length() / 2));
		attron(A_BOLD);
		if (start_quit == 0)
			attron(COLOR_PAIR(1));
		addstr(newgame.c_str());
		if (start_quit == 0)
			attroff(COLOR_PAIR(1));
		move((y / 2) + 1, (x / 2) - (quitgame.length() / 2));
		if (read_char == 27)
			break;
		else if (start_quit == 1)
			attron(COLOR_PAIR(1));
		addstr(quitgame.c_str());
		if (start_quit == 1)
			attroff(COLOR_PAIR(1));
		attroff(A_BOLD);
		read_char = getch();
		if (read_char == 259)
			start_quit = 0;
		else if (read_char == 258)
			start_quit = 1;
		else if (start_quit == 1 && (read_char == 10 || read_char == 32))
		{
			beep();
			flash();
			return (1);
			break;
		}
		else if (start_quit == 0 && (read_char == 10 || read_char == 32))
		{
			beep();
			flash();
			wclear(stdscr);
			return (0);
			break;
		}
		refresh();
	}
	return (0);
}

bool	Game::test_screen_size(int screen_current_x, int screen_current_y)
{
	if ((screen_current_x >= screenMin_X) && (screen_current_x <= screenMax_X))
		if ((screen_current_y >= screenMin_Y) && (screen_current_y <= screenMax_Y))
			return (true);
	return (false);
}

void	Game::mainGameLoopFunc(void)
{
	Player	*s = new Player();
	bool	paused = false;
	int		screen_current_x = 0;
	int		screen_current_y = 0;

	while (1)
	{
		getmaxyx(this->win, screen_current_y, screen_current_x);
		if (!test_screen_size(screen_current_x, screen_current_y))
		{
			while (!test_screen_size(screen_current_x, screen_current_y))
			{
				getmaxyx(this->win, screen_current_y, screen_current_x);
				wclear(this->win);
				move(screen_current_y / 2, (screen_current_x / 2) - 3);
				attron(A_BOLD);
				addstr("PAUSED");
				attroff(A_BOLD);
				move((screen_current_y / 2) + 1, (screen_current_x / 2) - 8);
				addstr("SCREEN TOO SMALL");
				move((screen_current_y / 2) + 2, (screen_current_x / 2) - 6);
				addstr("PLEASE RESIZE");
				refresh();
			}
			paused = true;
		}
		if (paused)
		{
			paused = false;
			if (GameMenu("RESUME"))
				break ;
		}
		if (s->collision())
		{
			move(12,36);
			printw("game over");
			break;
		}
		s->movesnake();
		if (s->getdir() == 'q')
			break;
		usleep(s->getspeed());
	}
	delete (s);
}

void	Game::run(void)
{
	bool	mainGameLoop = true;
	bool	retryGameLoop = true;

	if (!GameMenu("START GAME"))
	{
		retryGameLoop = true;
		while(retryGameLoop)
		{
			this->score = 0;
			mainGameLoop = true;
			if (mainGameLoop)
			{
				mainGameLoopFunc();
				mainGameLoop = false;
			}
			if (GameMenu("RETRY"))
			{
				retryGameLoop = false;
			}
		}
	}
}

int		Game::getWin_x(void) const
{
	return (this->win_x);
}

int		Game::getWin_y(void) const
{
	return (this->win_y);
}

void	Game::setWin_x(int const win_x)
{
	this->win_x = win_x;
}

void	Game::setWin_y(int const win_y)
{
	this->win_y = win_y;
}

void	Game::close(void)
{
	endwin();
}
