#include "Player.class.hpp"

snakepart::snakepart(int col, int row)
{
	x = col;
	y = row;
}

snakepart::snakepart()
{
	x = 0;
	y = 0;
}

void Player::remove_end()
{
	move(snake[snake.size()-1].y,snake[snake.size()-1].x);
	addch(' ');
	refresh();
	snake.pop_back();
}

void Player::draw_HT()
{
	attroff(COLOR_PAIR(3));
	move(snake[1].y,snake[1].x);
	addch(partchar);
	move(snake[0].y,snake[0].x);
	attron(COLOR_PAIR(3));
	addch(head);
	attroff(COLOR_PAIR(3));
	refresh();
}


void Player::graphics()
{
	init_pair(1, COLOR_GREEN, COLOR_BLACK);//--boarder
	init_pair(2, COLOR_BLUE, COLOR_BLACK);//--boarder
	init_pair(3, COLOR_RED, COLOR_BLACK);//--for head of snake
	init_pair(4, COLOR_CYAN, COLOR_BLACK);//--for food
	////--for the snake
	head = '@';
	partchar = 'x';
	oldchar = '#';
	_food = '*';

	//-----edge of the game
	for (int i =0;i<maxWidth-1;i++)
	{
		attron(COLOR_PAIR(2));
		move(0,i);
		addch(oldchar);
	}
	
	for (int i =0;i<maxWidth-1;i++)
	{
		attron(COLOR_PAIR(2));
		move(maxHeight-2,i);
		addch(oldchar);
	}
	
	for (int i =0;i<maxHeight-1;i++)
	{
		attron(COLOR_PAIR(1));
		move(i,0);
		addch(oldchar);
	}
	
	for (int i =0;i<maxHeight-1;i++)
	{
		attron(COLOR_PAIR(1));
		move(i,maxWidth -2);
		addch(oldchar);
	}
}

Player::Player()
{
	getmaxyx(stdscr, maxHeight,maxWidth);
	graphics();
	attroff(COLOR_PAIR(1));
	for (int i =0; i<5;i++)
		snake.push_back(snakepart(40+i,10));
	points = 0;
	speed = 110000;
	get = false;
	dir = 'l';
	srand(time(0));
	putfood();

	///--------draw of snake

	for (int i=0;i<snake.size();i++)
	{
			move(snake[i].y,snake[i].x);
			addch(partchar);
	}

	//----Write the points
	move(maxHeight-1,0);
	printw("%d",points);
	//----draw points
	move(food.y,food.x);
	addch(_food);
	refresh();

}

Player	&Player::operator=(Player const &rhs)
{
	(void)rhs;
	return (*this);
}

Player::~Player()
{

}

int Player::getspeed()
{
	return this->speed;
}

void Player::putfood()
{
	while (1)
	{
		int tmpx = rand() % maxWidth + 1;
		int tmpy = rand() % maxHeight + 1;
		for (int i =0; i<snake.size();i++)
			if ((snake[i].x == tmpx) && (snake[i].y == tmpy))
				continue;
		if (tmpx >= maxWidth -2 || tmpy >=  maxHeight -3)
			continue;
		food.x = tmpx;
		food.y = tmpy;
		break;
	}
	move(food.y,food.x);
	addch(_food);
	refresh();
}

bool Player::collision()
{
	if (snake[0].x ==0 || snake[0].x == maxWidth -1 || 
	snake[0].y == 0 || snake[0].y == maxHeight - 2)
		return true;
	for (int i = 2;i<snake.size();i++)
		if (snake[0].x == snake[i].x && snake[0].y == snake[i].y)
			return true;
	if ((snake[0].x == food.x) && (snake[0].y == food.y))
	{
		get = true;
		putfood();
		points+=10;
		move(maxHeight -1, 0);
		printw("%d", points);
		if ((points % 50) == 0)
			speed-=20000;
	}
	else
		get = false;
	return false;
}

void Player::movesnake()
{
	int tmp = getch();
	switch(tmp)
	{
		case KEY_LEFT:
			if (dir != 'r')
				dir = 'l';
			break;
		case KEY_UP:
			if (dir != 'd')
				dir = 'u';
			break;
		case KEY_DOWN:
			if (dir != 'u')
				dir = 'd';
			break;
		case KEY_RIGHT:
			if (dir != 'l')
				dir = 'r';
			break;
		case KEY_BACKSPACE:
			dir = 'q';
			break;
	}

	if (!get)
	{
		remove_end();
	}

	if (dir == 'l')
		snake.insert(snake.begin(), snakepart(snake[0].x-1, snake[0].y));
	else if (dir == 'r')
		snake.insert(snake.begin(), snakepart(snake[0].x+1, snake[0].y));
	else if (dir == 'u')
		snake.insert(snake.begin(), snakepart(snake[0].x, snake[0].y - 1));
	else if (dir == 'd')
		snake.insert(snake.begin(), snakepart(snake[0].x, snake[0].y + 1));
	draw_HT();
}

void Player::start()
{
}

char Player::getdir()
{
	return this->dir;
}