#include <ncurses.h>
#include <unistd.h>
#include <iostream>
#include "Player.class.hpp"
#include "Game.class.hpp"
#include "Exceptions.hpp"
#include "nibbler.hpp"

static bool	is_digits_only(const std::string str)
{
	return str.find_first_not_of("0123456789") == std::string::npos;
}

static bool	test_valid_size(std::string size_x, std::string size_y)
{
	if (is_digits_only(size_x) && is_digits_only(size_y))
	{
		if ((stoi(size_x) >= screenMin_X) && (stoi(size_x) <= screenMax_X))
		{
			if ((stoi(size_y) >= screenMin_Y) && (stoi(size_y) <= screenMax_Y))
				return (true);
			else
				throw Exceptions::ArgumentSizeOutOfRange();
		}
		else
			throw Exceptions::ArgumentSizeOutOfRange();
	}
	return (false);
}

static char	*char_str_to_lower(char *str)
{
	int	iterator;
	int	len;

	iterator = 0;
	len = std::strlen(str);
	while (iterator < len)
	{
		str[iterator] = std::tolower(str[iterator]);
		iterator++;
	}
	return (str);
}

static int	test_arguments(int argc, char **argv)
{
	try
	{
		if (argc < 2)
			throw Exceptions::NoArguments();
		else if (argc == 2)
		{
			if (std::strcmp("-help", char_str_to_lower(argv[1])) == 0)
			{
				std::cout << "[INFO]\tUsage: Executable + size-X + size-Y." << std::endl;
				std::cout << "\tExample: \"./nibbler 80 24\"" << std::endl << std::endl;
				std::cout << "Minimum:" << std::endl;
				std::cout << "\tX:" << screenMin_X << std::endl;
				std::cout << "\tY:" << screenMin_Y << std::endl;
				std::cout << "Maximum:" << std::endl;
				std::cout << "\tX:" << screenMax_X << std::endl;
				std::cout << "\tY:" << screenMax_Y << std::endl;
				return (0);
			}
			else
				throw Exceptions::InvalidArgument();
		}
		else if (argc == 3)
		{
			if (test_valid_size(argv[1], argv[2]) == true)
				return (1);
			else
				throw Exceptions::InvalidArgument();
		}
	}
	catch (std::exception &e)
	{
		std::cerr << "[ERROR]: " << e.what() << std::endl;
		return (-1);
	}
	return (-1);
}

int	main(int argc, char **argv)
{
	int	valid_arguments = test_arguments(argc, argv);

	if (valid_arguments != 1)
		return (valid_arguments);

	Game	game(argv[1], argv[2]);

	if (!game.getInit())
	{
		try
		{
			game.init();
		}
		catch (std::exception &e)
		{
			std::cerr << "[ERROR]: " << e.what() << std::endl;
			return (-1);
		}
	}
	game.run();
	game.close();
	return (0);
}