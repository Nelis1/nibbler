/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dlib2.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/24 14:39:38 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/24 14:53:28 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "dlib2.hpp"

void SayHelloString(const std::string name) {
	std::cout << "aHello " << name << " have a nice day! STRING" << std::endl;
}
