/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dlib2.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/24 14:41:15 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/24 14:53:20 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DLIB2_HPP
# define DLIB2_HPP

# ifdef __cplusplus
#  include <string>
#  include <iostream>
extern "C"
{
# endif

	void SayHelloString(const std::string name);

# ifdef __cplusplus
}
# endif

#endif
