/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dlib3.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/24 14:39:38 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/24 14:54:04 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "dlib3.hpp"

void SayHelloString(const std::string name) {
	std::cout << "aHello " << name << " have a nice day! STRING" << std::endl;
}
