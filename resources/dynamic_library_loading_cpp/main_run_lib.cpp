#include <iostream>
#include <dlfcn.h>
// #include <string>

typedef void (*PFN_SAY_NAME)(const char*);
typedef int (*square_ptr)(int);

int main(void) {
    void     (*print_string)(const std::string);
    void* handle = dlopen("./libDynamicLibrary.so", RTLD_LAZY);
    if (!handle) {
        std::cout << "Could not open the library" << std::endl;
        return 1;
    }

    print_string = (void(*)(const std::string)) dlsym(handle, "SayHelloString");
    if (!print_string) {
        std::cout << "Could not find symbol SayHelloString" << dlerror() << std::endl;
        dlclose(handle);
        return 1;
    }
    print_string("Nelis");

    PFN_SAY_NAME hello = reinterpret_cast<PFN_SAY_NAME>(dlsym(handle, "SayHello"));
    if (!hello) {
        std::cout << "Could not find symbol SayHello" << std::endl;
        dlclose(handle);
        return 1;
    }

    hello("Edoren");
    square_ptr count = reinterpret_cast<square_ptr>(dlsym(handle, "square"));
    if (!count) {
        std::cout << "Could not find symbol SayHello" << std::endl;
        dlclose(handle);
        return 1;
    }
    std::cout << count(5) << std::endl;
    dlclose(handle);

    return 0;
}
