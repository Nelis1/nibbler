#include "dlib.hpp"
#include <iostream>

void SayHelloString(const std::string name) {
	std::cout << "Hello " << name << " have a nice day! STRING" << std::endl;
}

void SayHello(const char* name) {
	std::cout << "Hello " << name << " have a nice day!" << std::endl;
}

int square(int n)
{
	return (n * n + 2);
}
