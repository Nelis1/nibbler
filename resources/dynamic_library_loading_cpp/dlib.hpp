#pragma once

#ifdef  __cplusplus
#include <string>
extern "C" {
#endif

void SayHelloString(const std::string name);
void SayHello(const char* name);
int square(int n);

#ifdef  __cplusplus
}
#endif
