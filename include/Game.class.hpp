/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Game.class.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 11:20:47 by cnolte            #+#    #+#             */
/*   Updated: 2018/06/11 11:49:11 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAME_CLASS_HPP
# define GAME_CLASS_HPP

# include <iostream>
# include <ncurses.h>
# include <unistd.h>
# include "Player.class.hpp"
# include <dlfcn.h>

# define library1_location "./libraries/lib1/"
# define library2_location "./libraries/lib2/"
# define library3_location "./libraries/lib3/"

# define library1_name "libDynamicLibrary1.so"
# define library2_name "libDynamicLibrary2.so"
# define library3_name "libDynamicLibrary3.so"

# define library1 library1_location library1_name
# define library2 library2_location library2_name
# define library3 library3_location library3_name

class Game
{
	public:
		//start canonical form
		Game(void);
		Game(std::string size_x, std::string size_y);
		Game(Game const & src);
		~Game(void);

		Game	&operator=(Game const &rhs);
		//end canonical form

		int		getInit(void) const;
		void	init(void);
		void	run(void);
		void	close(void);
		int		getWin_x(void) const;
		int		getWin_y(void) const;
		void	setWin_x(int const win_x);
		void	setWin_y(int const win_y);
	private:
		bool	test_screen_size(int screen_current_x, int screen_current_y);
		WINDOW	*win;
		int		win_x;
		int		win_y;
		int		score;

		//Libraries--------------------------------------------------------------
		void	*lib1_handle;
		void	(*print_string1)(const std::string);
		void	*lib2_handle;
		void	(*print_string2)(const std::string);
		void	*lib3_handle;
		void	(*print_string3)(const std::string);

		//private prototypes
		void	mainGameLoopFunc(void);
		void	check_and_load_libraries(void);
};

#endif
