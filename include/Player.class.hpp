#ifndef PLAYER_CLASS_HPP
# define PLAYER_CLASS_HPP

#include <iostream>
#include <vector>
#include <ncurses.h>
#include <cstdlib>
#include <unistd.h>

struct snakepart
{
	int x,y;
	snakepart(int col,int row);
	snakepart();
};

class Player
{
	public:
		Player();
		Player &operator=(Player const &rhs);
		~Player();
		void start();
		void putfood();
		bool collision();
		void movesnake();
		char getdir();
		int getspeed();
		void graphics();
		void remove_end();
		void draw_HT();

	private:
		int points;
		int speed;
		int maxWidth;
		int maxHeight;
		int _foo;
		char dir;
		char head;
		char partchar;
		char oldchar;
		char _food;
		bool get;
		snakepart food;
		std::vector<snakepart> snake;
};

#endif
