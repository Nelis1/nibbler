# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/05/29 23:46:32 by cnolte            #+#    #+#              #
#    Updated: 2018/07/25 10:06:27 by cnolte           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME := nibbler libraries/lib1/libDynamicLibrary1.so libraries/lib2/libDynamicLibrary2.so libraries/lib3/libDynamicLibrary3.so

SRCS := $(wildcard source/*.cpp) $(wildcard source/classes/*.cpp)
OBJS := $(SRCS:.cpp=.o)

CXX := clang++ -std=c++11
# CXXFLAGS := -Wall -Werror -Wextra
LIB := -lncurses -ldl

INC_DIRS = include/ libraries/lib1/include/ libraries/lib2/include/ libraries/lib3/include/
CXXFLAGS += $(addprefix -I, $(INC_DIRS))

DLIBRARIES = libraries/lib1/source/dlib1.o libraries/lib2/source/dlib2.o libraries/lib3/source/dlib3.o

$(NAME): $(OBJS) $(DLIBRARIES)
	@$(MAKE) -C libraries all
	$(CXX) $(OBJS) -o $@ $(LIB)
	@echo [INFO] $@ "compiled successfully."

all: $(NAME)

clean:
	@$(MAKE) -C libraries clean
	@rm -rf $(OBJS)
	@echo "[INFO] Objects removed!"

fclean: clean
	@$(MAKE) -C libraries fclean
	@rm -rf $(NAME)
	@echo "[INFO] $(NAME) removed!"
	@rm -rf input.tmp

re: fclean all

.PHONY: all clean fclean re
